#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#define CACHE_PATH "./cache/"

// Fonction envoyant un message d'erreur et terminant le programme avec code erreur.
void error(const char *msg);
// Fonction qui permettra les connections simultanées
void *connection_handler(void *);
// Fonction permettant de savoir si un fichier est présent dans le cache
int is_in_cache(char *filename);
void write_in_cache(char *filename, char *content);
char* get_file_last_modif_time(char *filename);
int html_code(char *response);
char *get_html_page_in_cache(char *filename);
void do_request_http(char *request, char *client_message, char *response, int server_sock, int sock, char *page);

int main (int argc, char *argv[]){
	int master_client_sock, client_com_sock, port;
	struct sockaddr_in clientaddr;
	struct sockaddr_in csin;
	int sinsize = sizeof csin;

	// Création du socket client
	master_client_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (master_client_sock < 0)
		error("Impossible de créer le socket");

	// Attention le numéro du port doit être le même que celui du client
	printf("%s","Choisissez un port : " );
	scanf("%i",&port);
	// Initialisation du socket pour la réception des infos du client
	bzero((char *) &clientaddr, sizeof(clientaddr));
	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(port);
	clientaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	// Bind du socket client (le rend fonctionnel !)
	bind(master_client_sock,(struct sockaddr *) &clientaddr, sizeof clientaddr);

	// Création du socket qui permettra d'envoyer/recevoir les informations du client
	listen(master_client_sock, 5); // 5 connexion simultanées max
	pthread_t thread_id;

	// Création du répertoire du cache s'il n'est pas encore présent
	if (access(CACHE_PATH, F_OK) == -1)
	// Tous les droits pour l'owner, on retire execute pour le groupe
	// on retire execute et write pour les autres
		mkdir(CACHE_PATH,0777);

	while(1){
		// Création d'un socket de communication avec le client
		client_com_sock = accept(master_client_sock, (struct sockaddr *) &csin,(socklen_t*) &sinsize);
		// Création du thread qui permettra les connections simultanées
		if(pthread_create(&thread_id, NULL, connection_handler, (void*)&client_com_sock) < 0){
			error("Création de thread impossible");
			return 1;
		}
	}
}


void *connection_handler(void *socket_desc){
    //Récupération du descripteur du socket
    int sock = *(int*)socket_desc;
	int server_sock;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char request[1024] , client_message[1024], response[1048576];
    char *page= "/index.html";
    int code;

    // Réception du message du client
    recv(sock, client_message, 1024, 0);

	// Création du socket serveur
	server_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (server_sock < 0)
		error("Impossible de créer le socket");

	// Initialisation du socket pour la connexion au serveur
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(80);
	server = gethostbyname(client_message);
	if (server == NULL)
		error("Serveur : Echec récupérations données de connexion du serveur");
	bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);

    // Connexion au serveur + ré-initialisation de request
	bzero(request, 1024);
	if (connect(server_sock, (struct sockaddr *) &serveraddr ,sizeof(struct sockaddr)) < 0)
		error("Serveur : Connection à l'hôte Impossible");

    // Si la page est présente dans le cache, on va interroger le serveur pour
    // savoir si notre page en cache est toujours à jour ou non
	if (is_in_cache(client_message) != -1){

		// Récupération de la date de mise à jour du cache
		char *date = get_file_last_modif_time(client_message);

		// Création et envoi de la requête pour savoir si une modification a été faite sur la page
		sprintf(request, "GET %s HTTP/1.1\r\nHost: %s\r\nIf-modified-since: %s\r\n\r\n", page, client_message, date);
		if (send(server_sock, request, strlen(request), 0) < 0)
			error("Serveur : Erreur lors de l'envoi de la requête au serveur");

		bzero(request, 1024);
		recv(server_sock, request, 1024, 0);
		code = html_code(request);
    	// Si la page n'a pas modifiée, on la récupère dans le cache
    	if (code == 304){
    		char *buff = calloc (1048576, sizeof(char));
    		buff = get_html_page_in_cache(client_message);
			if (send(sock,buff,strlen(buff),0) < 0)
				error("Serveur : Erreur lors de l'envoi de la réponse au client");
			return 0;    		
    	}
		// Sinon, on récup la page et on overwrite le cache
    	else{
    		do_request_http(request, client_message, response, server_sock, sock, page);	   		
    	}
    }

    // Si la page n'est pas présente dans le cache, on la récupère sur le serveur
    // et on la met dans le cache.
	else{
		do_request_http(request, client_message, response, server_sock, sock, page);
	}
}


int is_in_cache(char *filename){
	char *path = calloc (100, sizeof(char));
	strcat(path, CACHE_PATH);
	strcat(path, filename);
	//F_OK teste l'existence du fichier
	if(access(path, F_OK ) != -1)
		return 0;
	return -1;
}

void write_in_cache(char *filename, char *content){
	char *path = calloc (100, sizeof(char));
	FILE *fp;
	strcat(path, CACHE_PATH);
	strcat(path, filename);
	fp = fopen(path, "w+");
	fputs(content, fp);
	fclose(fp);	
}

char* get_file_last_modif_time(char *filename){
	char *path = calloc (100, sizeof(char));
	strcat(path, CACHE_PATH);
	strcat(path, filename);
	struct stat informations;
	stat(path, &informations);
	char *date = calloc(100, sizeof(char));
	strftime(date, 100, "%a, %d %b %Y %X %Z", gmtime(&(informations.st_mtime)));
	return date;
}

int html_code(char *chaine){
	int nbr = 0;
	sscanf(chaine, "HTTP/1.1 %i",&nbr);
	return nbr;
}

char *get_html_page_in_cache(char *filename){
	char *path = calloc (100, sizeof(char));
	strcat(path, CACHE_PATH);
	strcat(path, filename);
	FILE *f = fopen(path, "rb");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);
	char *content = malloc(fsize + 1);
	fread(content, fsize, 1, f);
	fclose(f);
	content[fsize] = 0;
	return content;

}

void do_request_http(char *request, char *client_message, char *response, int server_sock, int sock, char *page){
	sprintf(request, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", page, client_message);
		if (send(server_sock, request, strlen(request), 0) < 0)
			error("Serveur : Erreur lors de l'envoi de la requête au serveur");

		// Récupération de la réponse du serveur http
		bzero(response, 1048576);
		recv(server_sock, response, 1048576, 0);

		// Ecriture dans le cache de la page + envoi de la réponse au client
		write_in_cache(client_message, response);
		if (send(sock,response,strlen(response),0) < 0)
			error("Serveur : Erreur lors de l'envoi de la réponse au client");		   		
}

void error(const char *msg){
	perror(msg);
	exit(1);
}