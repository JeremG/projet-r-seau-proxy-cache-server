#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>

// Fonction envoyant un message d'erreur et terminant le programme avec code erreur.
void error(const char *msg);

int main (int argc, char *argv[]){
	char *host;
	char *page;
	char request[1000];
	int my_sock;
	int binary_adress;
	struct sockaddr_in serveraddr;
	struct hostent *server;

	host = "www.perdu.com";
	page = "/index.html";

	// Création du socket
	my_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (my_sock < 0)
		error("Impossible de créer le socket");

	// Initialisation du socket pour la connexion au serveur
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(80);
	server = gethostbyname(host);
	if (server == NULL)
		error("Echec récupérations données de connexion du serveur");
	bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);

	// Connexion socket
	bzero(request, 1000);
	if (connect(my_sock, (struct sockaddr *) &serveraddr ,sizeof(struct sockaddr)) < 0)
		error("Connection à l'hôte Impossible");
	sprintf(request, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n",page,host);

	// Envoi de la requête au serveur
	if (send(my_sock, request, strlen(request), 0) < 0)
	        error("Erreur lors de l'envoi de la requête au serveur");

	// Récupération de la réponse
	bzero(request, 1000);
    recv(my_sock, request, 2048, 0);
    printf("%s \n", request);

    close(my_sock);
}

void error(const char *msg){
    perror(msg);
    exit(1);
}