#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>

// Fonction envoyant un message d'erreur et terminant le programme avec code erreur.
void error(const char *msg);

int main (int argc, char *argv[]){

	char *host;
	int port;
	char request[1024], response[1048576]; // 1 ko, 1 Mo
	int my_sock;
	struct sockaddr_in serveraddr;
	struct hostent *server;

	host = "127.0.0.1";

	// Création du socket
	my_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (my_sock < 0)
		error("Client :Impossible de créer le socket");

	// Attention le numéro du port pour doit être le même que celui du serveur
	printf("%s", "Choisissez un port : " );
	scanf("%i", &port);
	// Initialisation du socket pour la connexion au serveur local
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(port);
	server = gethostbyname(host);
	if (server == NULL)
		error("Client :Echec récupérations données de connexion du serveur");
	bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);

	// Connexion socket
	if (connect(my_sock, (struct sockaddr *) &serveraddr ,sizeof(struct sockaddr)) < 0)
		error("Client :Connection à l'hôte Impossible");

	// Récupération de l'adresse à laquelle le client veut accéder
	bzero(request, 1024);
	sprintf(request, "%s",argv[1]);

	// Envoi de la requête au serveur local
	if (send(my_sock, request, strlen(request), 0) < 0)
	        error("Client :Erreur lors de l'envoi de la requête au serveur");

	// Récupération de la réponse du serveur local
	bzero(response, 1048576);
    recv(my_sock, response, 1048576, 0);
    printf("%s \n", response);

    close(my_sock);
}

void error(const char *msg){
    perror(msg);
    exit(1);
}
